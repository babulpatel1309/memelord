import 'package:flutter/material.dart';
import 'package:memelord/data/card_model.dart';
import 'package:memelord/utils/firebaseutils.dart';
import 'package:memelord/utils/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'colors.dart';
import 'constants.dart';

customButton(String title,
    {gradientColor = skyBlueWhiteGradient, textColor = white}) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(30, 20, 30, 0),
    child: Card(
      elevation: 5.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(30.0))),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: gradientColor,
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text(title,
              style: TextStyle(
                  color: textColor, fontSize: 20, fontWeight: FontWeight.bold)),
        ),
      ),
    ),
  );
}

generateDummyData() {
  var cardList = List<CardModel>();
  for (int index = 0; index < 15; index++) {
    CardModel cardModel = CardModel();
    cardModel.imagePath =
        "https://tallypress.com/wp-content/uploads/2016/12/10-Reasons-Why-Girls-Look-Up-To-Bollywood-Actress-Deepika-Padukone-1.jpg";
    cardModel.title = "Dipu";
    cardModel.subTitle = "Funny meme";
    cardModel.likes = (index + 300);
    cardList.add(cardModel);
  }

  return cardList;
}

showLoading(context, {loadingText = "Loading..."}) {
  try {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        title: null,
        contentPadding: EdgeInsets.all(0.0),
        elevation: 10,
        titlePadding: EdgeInsets.all(0.0),
        content: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              SizedBox(
                width: 10,
              ),
              new Text(loadingText),
            ],
          ),
        ),
      ),
    );
  } catch (e) {
    print(e);
  }
}

Future<bool> checkLoggedinUser(context) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool loggedIn = prefs.getBool(PREFS_LOGIN);
  if (loggedIn != null && loggedIn) {
    String phoneNo = prefs.getString(PREF_PHONE);
    if (userData == null) await checkUserExist(phoneNo);
  } else {
    Navigator.of(context).pushNamed(registerScreen);
  }

  return loggedIn;
}