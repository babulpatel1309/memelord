import 'package:flutter/material.dart';

import 'dimens.dart';

const textHeading = TextStyle(
  fontSize: textSize1,
);
const textHeading2 = TextStyle(
  fontSize:  textSize2
);
const textHeading3 = TextStyle(
    fontSize:  textSize3
);
const textHeading4 = TextStyle(
    fontSize:  textSize4
);
const textHeading5 = TextStyle(
    fontSize:  textSize5
);
const textHeading6 = TextStyle(
    fontSize:  textSize6
);
const textHeading4Bold = TextStyle(
    fontSize:  textSize4,
  fontWeight: FontWeight.w400
);