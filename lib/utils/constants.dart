/*API Helper constants*/

const API_LOGIN = "login";

const PREFS_LOGIN = 'loggedIn';
const PREFS_AGREEMENT = "licence_agreement";
const FONT_TIMES_NEW_ROMAN = "TimesNewRoman";
const PREF_PHONE = 'phone_no';

const PARAM_TEL = 'telNo';
const PARAM_UID = 'uid';

const PARAM_LIKES = 'likes';
const PARAM_DISLIKES = 'dislikes';
const PARAM_TIMESTAMP = 'timestamp';
const PARAM_IMAGEURL = 'imageURL';

const PARAM_TRENDING = 'trending';
