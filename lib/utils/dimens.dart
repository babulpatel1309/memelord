import 'package:flutter/widgets.dart';

const double textSize1 = 22;
const double textSize2 = 20;
const double textSize3 = 18;
const double textSize4 = 16;
const double textSize5 = 14;
const double textSize6 = 12;
const double textSize7 = 8;



const double homeScreenToolbarHeight = 200;
const double homeScreenImageHeight = 160;
const double homeScreenTopTabHeight = 80;
const double screenHorizontalMargin = 16;
const double toolbarHeight = 80;
const double cardDropShadowRadius = 10.0;

