import 'package:flutter/material.dart';

Gradient fabButtonGradient = LinearGradient(
  colors: [
    Color(0xffff8923),
    Color(0xfffda91b),
  ],
  begin: FractionalOffset.centerLeft,
  end: FractionalOffset.centerRight,
);

const buttonColor = Color(0xff1a2834);
const textFieldColor = Color(0xff1a2834);
const toolbarColor = Color(0xff3cc0b4);
const lineColor = Color(0xff3cc0b4);
const blueColor = Color(0xff0645AD);


const Gradient skyBlueWhiteGradient = LinearGradient(
  colors: [
    Color(0xffFFFFFF),
    Color(0xff30C1FE),
  ],
  begin: FractionalOffset.centerLeft,
  end: FractionalOffset.centerRight,
);

const Gradient whiteGradient = LinearGradient(
  colors: [
    Color(0xffFFFFFF),
    Color(0xffFFFFFF),
  ],
  begin: FractionalOffset.centerLeft,
  end: FractionalOffset.centerRight,
);


const Gradient redGradient = LinearGradient(
  colors: [
    Color(0xffE25320),
    Color(0xffE25320),
  ],
  begin: FractionalOffset.centerLeft,
  end: FractionalOffset.centerRight,
);

const Gradient yellowGradient = LinearGradient(
  colors: [
    Color(0xffFFE878),
    Color(0xffFFFFFF),
    Color(0xffFFE878),
  ],
  begin: FractionalOffset.centerLeft,
  end: FractionalOffset.centerRight,
);


const Gradient silverGradient = LinearGradient(
  colors: [
    Color(0xffD6D6D6),
    Color(0xffFFFFFF),
    Color(0xffD6D6D6),
  ],
  begin: FractionalOffset.centerLeft,
  end: FractionalOffset.centerRight,
);

const Gradient orangeGradient = LinearGradient(
  colors: [
    Color(0xffEBB772),
    Color(0xffFFFFFF),
    Color(0xffEBB772),
  ],
  begin: FractionalOffset.centerLeft,
  end: FractionalOffset.centerRight,
);


const skyBlue = Color(0xff30C1FE);

const cursorColor = Color(0xffa2b8ca);
const accentColor = Color(0xff1a2834);
const hintColor = Color(0xFF808080);
const blueText = Color(0xFF1979E0);

const homeTabSelectedTabColor = Colors.white;
const homeTabUnselectedTabColor = Color(0xFF8ba4bd);
const windowBackground = Color(0xFFe5e5e5);
const boxBorder = Color(0xFF541E8A);
const loginScreenDropDownBorder = Color(0xFFe7e9e9);
const loginScreenBlueTextColor = Color(0xff1a2834);
const loginScreenLoginWithLabelTextColor = Color(0xFF8ba4bd);
const loginScreenPasswordIconHidePasswordColor = Color(0xFF8ba4bd);
const loginScreenPasswordIconShowPasswordColor = Color(0xFF1a2834);
const loginScreenOrDivider = Color(0xFFe3ecf1);
const loginScreenOrTextColor = Color(0xffa2b8ca);
const greyLight = Color(0xff9ba4aa);
const greyDark = Color(0xff9ba4ab);
const yellowColor = Color(0xff1a2834);
const yellowLightColor = Color(0xff3cc0b4);
const chipColor = Color(0xfff0f9ff);
const windowBackgroundLightBlue = Color(0xfff5fdff);
const cardContainerBackdrop = Color(0xFFEBF6FB);
const white = Color(0xFFFFFFFF);
const black = Color(0xFF000000);
const blueButtons = Color(0xff1a2834);
const redText = Color(0xFFDb2317);
const redDarkText = Color(0xFFa51b12);
const orangeText = Color(0xFF3cc0b4);
const greyFeedbackTextField = Color(0xFFE9F2F7);
const onlineGreen = Color(0xFF1EE27A);
const mapBlue = Color(0xff1a2834);
const arrowGray = Color(0xFF889FB7);
const arrowGray1 = Color(0xFFC5D1DE);
const arrowGray2 = Color(0xFFE8EDF2);
const missionDeclinedColor = Color(0xffDD2216);
const missionAcceptColor = Color(0xff1a2834);
const imageBlurShadow = Color(0x50335FB8);
const blueLight = Color(0xff1a2834);
const progressbarBackground = Color(0xffCEE9F9);
const bookmarkTabsDropShadow = Color(0x99E9F3F9);