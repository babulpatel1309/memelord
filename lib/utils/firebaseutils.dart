import 'dart:convert';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:memelord/data/memedata.dart';
import 'package:memelord/data/userdata.dart';
import 'package:memelord/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

_getStorageReference() {
  return FirebaseStorage.instance;
}

FirebaseDatabase _firebaseDatabase;

_getDatabaseReference() {
  if (_firebaseDatabase == null) {
    _firebaseDatabase = FirebaseDatabase.instance;
    _firebaseDatabase.setPersistenceEnabled(true);
    _firebaseDatabase.setPersistenceCacheSizeBytes(10000000);
  }

  return _firebaseDatabase;
}

FirebaseStorage storage;
UserData userData;

_initFirebaseStorage() async {
  FirebaseApp app = await FirebaseApp.configure(
      name: 'memelord',
      options: FirebaseOptions(
          googleAppID: Platform.isIOS ? '' : '1:811499702506:android:f5c8144296cd9064',
          apiKey: 'AIzaSyAHeB_0KBqG3bXP5nOqegCI7cGoyaEV0Bs',
          projectID: 'projecttango-5e03d'));

  storage = FirebaseStorage(app: app, storageBucket: 'gs://projecttango-5e03d.appspot.com/');
}

uploadImage(File imageFile) async {
  if (storage == null) await _initFirebaseStorage();
  int currentTimeStamp = DateTime.now().millisecondsSinceEpoch;

  StorageReference storageReference = storage.ref().child("memeloard/" + currentTimeStamp.toString() + ".jpg");

  StorageUploadTask uploadTask = storageReference.putFile(imageFile);
  String imageURL = await (await uploadTask.onComplete).ref.getDownloadURL();
  print('Uploaded URL $imageURL');

  MemeData data = MemeData();
  data.telNo = userData.telNo;
  data.uid = userData.uid;
  data.likes = 0;
  data.dislikes = 0;
  data.imageURL = imageURL;
  data.timestamp = currentTimeStamp;

  return await addEntryToDB(data);
}

addVerifiedPhoneNo(data) async {
  if (storage == null) await _initFirebaseStorage();
  _getDatabaseReference();

  DatabaseReference databaseReference = _firebaseDatabase.reference().child('memeloard').child('users');

  await databaseReference.push().set(data);
}

Future<bool> checkUserExist(String phoneNo) async {
  if (storage == null) await _initFirebaseStorage();
  _getDatabaseReference();

  DatabaseReference databaseReference = _firebaseDatabase.reference().child('memeloard').child('users');

  var snapShot = await databaseReference.orderByChild(PARAM_TEL).equalTo(phoneNo).once();

  if (snapShot != null && snapShot.value != null) {
    userData = UserData();
    var data = snapShot.value[snapShot.value.entries.elementAt(0).key];
    userData.uid = data['uid'];
    userData.telNo = data['telNo'];

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(PREFS_LOGIN, true);
    prefs.setString(PREF_PHONE, userData.telNo);
    return (snapShot.value != null);
  }
  return false;
}

addEntryToDB(MemeData data) async {
  _getDatabaseReference();

  DatabaseReference databaseReference = _firebaseDatabase.reference().child('memeloard').child('memes');

  return await databaseReference.push().set({
    PARAM_TEL: data.telNo,
    PARAM_UID: data.uid,
    PARAM_IMAGEURL: data.imageURL,
    PARAM_LIKES: data.likes,
    PARAM_DISLIKES: data.dislikes,
    PARAM_TIMESTAMP: data.timestamp
  });
}

Future<DataSnapshot> getImagesFromDB() async {
  _getDatabaseReference();

  DatabaseReference databaseReference = _firebaseDatabase.reference().child('memeloard').child('memes');

  return await databaseReference.orderByChild(PARAM_LIKES).once();
}

updatePointTable(MemeData data) async {
  _getDatabaseReference();

  DatabaseReference databaseReference = _firebaseDatabase.reference().child('memeloard').child('memes').child(data.key);

  await databaseReference.set(data.toJson());
}

Future<DataSnapshot> getImagesForUserDB() async {
  _getDatabaseReference();

  DatabaseReference databaseReference = _firebaseDatabase.reference();
  var query = databaseReference.child('memeloard').child('memes').orderByChild(PARAM_TEL).equalTo(userData.telNo);

  return await query.limitToFirst(20).once();
}

addTrendingEntryToDB() async {
  _getDatabaseReference();

  DatabaseReference databaseReference = _firebaseDatabase.reference().child('memeloard').child('trending');

  await databaseReference.remove();

  for(int index=1;index < 21;index++) {
    await databaseReference.push().set({PARAM_TRENDING: "meme $index", PARAM_UID: index,});
  }
}

Future<DataSnapshot> fetchTendingEntry() async{
  _getDatabaseReference();

  DatabaseReference databaseReference = _firebaseDatabase.reference();
  var query = databaseReference.child('memeloard').child('trending').orderByKey().once();
  return query;
}