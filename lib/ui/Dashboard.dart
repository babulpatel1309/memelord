import 'dart:io';

import 'package:flutter/material.dart';
import 'package:memelord/ui/tabs/TabMemes.dart';
import 'package:memelord/ui/tabs/TabProfile.dart';
import 'package:memelord/ui/tabs/TabTrending.dart';
import 'package:memelord/utils/colors.dart';
import 'package:memelord/utils/firebaseutils.dart';
import 'package:memelord/utils/images.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:memelord/utils/util.dart';

class DashboardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    print('UserData ---->>> $userData');
    return DashboardScreenState();
  }
}

class DashboardScreenState extends State<DashboardScreen> with SingleTickerProviderStateMixin {
  int selectedIndex = 0;

  var tabLists = <Widget>[TabMemes(), TabProfile(), TabTrending()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: tabLists[selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: _customIcon(icMemes, 0), title: Container(height: 0.0)),
          BottomNavigationBarItem(icon: _customIcon(icTrending, 1), title: Container(height: 0.0)),
          BottomNavigationBarItem(icon: _customIcon(icProfile, 2), title: Container(height: 0.0)),
        ],
        onTap: (index) {
          _handleBottomNavClick(index);
        },
        currentIndex: selectedIndex,
      ),
    );
  }

  Widget _customIcon(imageName, index) {
    return Container(
      child: Image.asset(
        imageName,
        height: selectedIndex == index ? 56 : 48,
      ),
    );
  }

  _handleBottomNavClick(index) async {
    if (index == 2) {
      bool isLoggedIn = await checkLoggedinUser(context);
      if (isLoggedIn == null || !isLoggedIn) return;
    }

    setState(() {
      selectedIndex = index;
    });
  }
}
