import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memelord/utils/constants.dart';
import 'package:memelord/utils/firebaseutils.dart';
import 'package:memelord/utils/images.dart';
import 'package:memelord/utils/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    _redirectToDashboard(context);

    return Scaffold(
      body: Container(
          child: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: double.infinity,
            child: Image.asset(
              bgImage,
              fit: BoxFit.cover,
            ),
          ),
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Image.asset(
                    logo,
                    height: 130,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50, left: 50, right: 50),
                  child: Image.asset(
                    memelordTitle,
                    height: 100,
                  ),
                )
              ],
            ),
          ),
        ],
      )),
    );
  }

  _redirectToDashboard(context) async {
//    addTrendingEntryToDB();
    await Future.delayed(Duration(milliseconds: 2000));
    Navigator.of(context).pushReplacementNamed(dashboardScreen);
  }


}
