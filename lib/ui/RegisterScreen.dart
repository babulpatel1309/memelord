import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:memelord/utils/colors.dart';
import 'package:memelord/utils/constants.dart';
import 'package:memelord/utils/firebaseutils.dart';
import 'package:memelord/utils/images.dart';
import 'package:memelord/utils/routes.dart';
import 'package:memelord/utils/util.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterScreenState();
  }
}

class RegisterScreenState extends State<RegisterScreen> {
  var screenType = ScreenType.register;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        phnControllerSignIn.text = "";
        otpController.text = "";
        phnControllerSignUp.text = "";

        if (screenType != ScreenType.register) {
          _handleScreenChange(ScreenType.register);
        } else {
          Navigator.pop(context);
        }
        return false;
      },
      child: Scaffold(
        key: _scaffoldKey,
        body: Container(
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: double.infinity,
                child: Image.asset(
                  icRegister,
                  fit: BoxFit.cover,
                ),
              ),
              SingleChildScrollView(
                child: _getAppropriateScreen(),
              )
            ],
          ),
        ),
      ),
    );
  }

  _getAppropriateScreen() {
    switch (screenType) {
      case ScreenType.register:
        return _inflateRegisterScreen();
      case ScreenType.signIn:
        return _inflateSignInScreen();
      case ScreenType.signUp:
        return _inflateSignUpScreen();
      case ScreenType.confirmOTP:
        return _inflateOTPScreen();
    }
  }

//TODO Inflate register screen
  _inflateRegisterScreen() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 100, 0, 10),
          child: Image.asset(
            logo,
            height: 70,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 50, left: 70, right: 70),
          child: Image.asset(
            memelordTitle,
            height: 100,
          ),
        ),
        InkWell(
          onTap: () {
            _handleScreenChange(ScreenType.signIn);
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
            child: customButton('Sign In',
                gradientColor: redGradient, textColor: skyBlue),
          ),
        ),
        InkWell(
          onTap: () {
            _handleScreenChange(ScreenType.signUp);
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 0, left: 20, right: 20),
            child: customButton('Sign Up'),
          ),
        )
      ],
    );
  }

  _handleScreenChange(ScreenType selectedScreenType) {
    setState(() {
      screenType = selectedScreenType;
    });
  }

//TODO Inflate login screen

  TextEditingController phnControllerSignIn = TextEditingController();

  _inflateSignInScreen() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 100, 0, 10),
          child: Image.asset(
            logo,
            height: 70,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 50, left: 70, right: 70),
          child: Image.asset(
            memelordTitle,
            height: 100,
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * .12,
        ),
        Text(
          "Write down your phone number",
          style: TextStyle(
              fontSize: 23, fontWeight: FontWeight.bold, color: white),
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(50, 20, 50, 0),
          child: Theme(
            data: ThemeData(
                primaryColor: white, accentColor: white, hintColor: white),
            child: TextField(
              controller: phnControllerSignIn,
              cursorColor: white,
              maxLength: 10,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
              style: TextStyle(color: white),
              decoration: InputDecoration(
                  hintText: "Phone number",
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: white))),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        InkWell(
          onTap: () {
            _handleSignInBtnClick();
          },
          child: customButton('Sign In'),
        ),
      ],
    );
  }

  _handleSignInBtnClick() async {
    if (phnControllerSignIn.text.trim().isEmpty ||
        phnControllerSignIn.text.trim().length < 10) {
      _showSnackBar('Please enter valid mobile number');
      return;
    }

    bool isUserExist =
        await checkUserExist('+91${phnControllerSignIn.text.trim()}');

    if (!isUserExist)
      _handleSignIn('+91${phnControllerSignIn.text.trim()}');
    else
      Navigator.of(context).pop();
//      Navigator.of(context).pushReplacementNamed(dashboardScreen);
  }

  //TODO Inflate signUp screen

  TextEditingController phnControllerSignUp = TextEditingController();

  _inflateSignUpScreen() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 100, 0, 10),
          child: Image.asset(
            logo,
            height: 70,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 50, left: 70, right: 70),
          child: Image.asset(
            memelordTitle,
            height: 100,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          "Write down your phone number",
          style: TextStyle(
              fontSize: 23, fontWeight: FontWeight.bold, color: white),
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(50, 20, 50, 0),
          child: Theme(
            data: ThemeData(
                primaryColor: white, accentColor: white, hintColor: white),
            child: TextField(
              controller: phnControllerSignUp,
              cursorColor: white,
              maxLength: 10,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
              style: TextStyle(color: white),
              decoration: InputDecoration(
                  hintText: "Phone number",
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: white))),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        InkWell(
          onTap: () {
            _handleNextBtnClick();
          },
          child: customButton('Next'),
        ),
      ],
    );
  }

  _handleNextBtnClick() {
    if (phnControllerSignUp.text.trim().isEmpty ||
        phnControllerSignUp.text.trim().length < 10) {
      _showSnackBar('Please enter valid mobile number');
      return;
    }
    _handleSignIn('+91${phnControllerSignUp.text.trim()}');
  }

  //TODO Inflate OTP screen
  TextEditingController otpController = TextEditingController();

  _inflateOTPScreen() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 100, 0, 10),
          child: Image.asset(
            logo,
            height: 70,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 50, left: 70, right: 70),
          child: Image.asset(
            memelordTitle,
            height: 100,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Confirm your phone number\ncode we sent you.",
            style: TextStyle(
                fontSize: 23, fontWeight: FontWeight.bold, color: white),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(50, 20, 50, 0),
          child: Theme(
            data: ThemeData(
                primaryColor: white, accentColor: white, hintColor: white),
            child: TextField(
              controller: otpController,
              cursorColor: white,
              maxLength: 6,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
              style: TextStyle(color: white),
              decoration: InputDecoration(
                  hintText: "Code",
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: white))),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        InkWell(
          onTap: () {
            _handleConfirmBtnClick();
          },
          child: customButton('Sign Up'),
        ),
      ],
    );
  }

  _handleConfirmBtnClick() {
    if (otpController.text.trim().isEmpty) {
      _showSnackBar('Please enter valid OTP');
      return;
    }

    showLoading(context);
    _verifyCode(otpController.text.trim());
//    _handleScreenChange(ScreenType.confirmOTP);
  }

  String verificationId;
  String phonNo;

  _handleSignIn(String phoneNO) async {
    phonNo = phoneNO;
    showLoading(context);

    final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verId) {
      this.verificationId = verId;
    };

    final PhoneCodeSent smsCodeSent = (String verId, [int forceCodeResend]) {
      Navigator.pop(context); //Dismiss alert dialog
      this.verificationId = verId;
      setState(() {
        screenType = ScreenType.confirmOTP;
      });
    };

    final PhoneVerificationFailed veriFailed = (AuthException exception) {
      Navigator.pop(context); //Dismiss alert dialog
      print('${exception.message}');
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNO,
        codeAutoRetrievalTimeout: autoRetrieve,
        codeSent: smsCodeSent,
        timeout: const Duration(seconds: 5),
        verificationCompleted: (verified) {
          print('verified $verified');
        },
        verificationFailed: veriFailed);
  }

  _verifyCode(code) {
    var credential = PhoneAuthProvider.getCredential(
        verificationId: verificationId, smsCode: code);
    _auth.signInWithCredential(credential).then((user) async {
      Navigator.pop(context); //Dismiss alert dialog
      print(user);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool(PREFS_LOGIN, true);
      prefs.setString(PREF_PHONE, phonNo);

      await addVerifiedPhoneNo({PARAM_TEL: phonNo, PARAM_UID: user.user.uid});

      Navigator.of(context).pushReplacementNamed(dashboardScreen);
    }).catchError((e) {
      Navigator.pop(context); //Dismiss alert dialog
      print(e);
    });
  }

  _showSnackBar(String content) {
    if (content == null) content = "Something went wrong..!!";
    var snackbar = SnackBar(
      content: Text(content),
      duration: Duration(milliseconds: 2000),
    );
    _scaffoldKey.currentState.showSnackBar(snackbar);
  }
}

enum ScreenType { register, signIn, signUp, confirmOTP }
