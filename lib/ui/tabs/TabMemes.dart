import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:memelord/data/card_model.dart';
import 'package:memelord/data/memedata.dart';
import 'package:memelord/ui/tabs/activeCard.dart';
import 'package:memelord/utils/firebaseutils.dart';
import 'package:memelord/utils/util.dart';
import 'package:path_provider/path_provider.dart';
import 'package:wc_flutter_share/wc_flutter_share.dart';

import '../../utils/colors.dart';
import '../../utils/images.dart';

class TabMemes extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TabMemesState();
  }
}

class TabMemesState extends State<TabMemes> with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  AnimationController _buttonController;
  int currentCardIndex = 0;

  Animation<double> rotate;
  Animation<double> right;
  Animation<double> bottom;
  Animation<double> width;
  int flag = 0;
  List selectedData = [];
  var listOfData = List<MemeData>();
  var isFetching = false;

  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      _loadData();
    });

    _buttonController = new AnimationController(duration: new Duration(milliseconds: 1000), vsync: this);

    rotate = new Tween<double>(
      begin: -0.0,
      end: -40.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    rotate.addListener(() {
      setState(() {
        if (rotate.isCompleted) {
          var i = listOfData.removeLast();
          listOfData.insert(0, i);

          _buttonController.reset();
        }
      });
    });

    right = new Tween<double>(
      begin: 0.0,
      end: 400.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    bottom = new Tween<double>(
      begin: 15.0,
      end: 100.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    width = new Tween<double>(
      begin: 20.0,
      end: 25.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.bounceOut,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            child: Image.asset(
              icMainFunny,
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            top: 60,
            left: 0,
            right: 0,
            child: Center(
              child: Image.asset(
                memelordTitle,
                width: MediaQuery.of(context).size.width * 0.6,
              ),
            ),
          ),
          _content(),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  _content() {
    return Container(
      margin: EdgeInsets.only(top: 120),
      padding: EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _middleTinderSwipes(),
          SizedBox(
            height: 10,
          ),
          _bottomShareClicks(),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  _middleTinderSwipes() {
    var timeDilation = 0.4;

    double initialBottom = 15.0;
    var dataLength = listOfData.length;
    double backCardPosition = initialBottom + (dataLength - 1) * 10 + 10;
    double backCardWidth = -10.0;
    List<Widget> cardList = new List();
    for (int x = 0; x < 3; x++) {
      cardList.add(Draggable(
        onDragEnd: (drag) {
          _onDislikeClick(currentCardIndex);
        },
        childWhenDragging: Container(),
        feedback: Card(
          elevation: 12,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            width: 240,
            height: 300,
          ),
        ),
        child: Card(
          elevation: 12,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            width: 240,
            height: 300,
          ),
        ),
      ));
    }

    return Expanded(
      child: Center(
        child: Container(
          margin: EdgeInsets.only(bottom: 10),
          child: listOfData.length > 0
              ? new Stack(
                  fit: StackFit.passthrough,
                  alignment: AlignmentDirectional.center,
                  children: listOfData.map(
                    (item) {
                      if (listOfData.indexOf(item) == dataLength - 1) {
                        return cardDemo(
                            item,
                            bottom.value,
                            right.value,
                            0.0,
                            0,
                            rotate.value,
                            rotate.value < -10 ? 0.1 : 0.0,
                            context,
                            dismissImg,
                            flag,
                            addImg,
                            swipeRight,
                            swipeLeft);
                      }else{
                        backCardPosition = backCardPosition + 10;
                        backCardWidth = backCardWidth + 10;
                        return cardNotActive(item, backCardPosition, 10.0, 10.0,
                            backCardWidth, 0.0, 0.0, context);
                      }
                    },
                  ).toList(),
                )
              : (isFetching
                  ? Container()
                  : Text(
                      "No Event Left",
                      style: new TextStyle(color: skyBlue, fontSize: 50.0),
                    )),
        ),
      ),
    );
  }

  _bottomShareClicks() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          child: Image.asset(
            icDislike,
            width: 60,
          ),
          onTap: () => _onDislikeClick(currentCardIndex),
        ),
        InkWell(child: Image.asset(icShare, width: 60), onTap: _onShareClick),
        InkWell(child: Image.asset(icLike, width: 60), onTap: _onLikeClick),
      ],
    );
  }

  _onDislikeClick(currentCardIndex) {
    swipeLeft();
    dismissImg(listOfData[currentCardIndex]);
  }

  _onShareClick() {
    shareImage(listOfData[currentCardIndex].imageURL);
  }

  _onLikeClick() {
    swipeRight();
    dismissImg(listOfData[currentCardIndex]);
  }

  dismissImg(MemeData img) async {
    if (img.likes > 0) img.likes = (img.likes - 1);
    await updatePointTable(img);

    setState(() {
      listOfData.remove(img);
    });
  }

  addImg(MemeData img) async {
    img.likes = (img.likes + 1);
    await updatePointTable(img);
    setState(() {
      listOfData.remove(img);
      selectedData.add(img);
    });
  }

  Future<Null> _swipeAnimation() async {
    try {
      await _buttonController.forward();
    } on TickerCanceled {}
  }

  swipeRight() {
    if (flag == 0)
      setState(() {
        flag = 1;
      });
    _swipeAnimation();
  }

  swipeLeft() {
    if (flag == 1)
      setState(() {
        flag = 0;
      });
    _swipeAnimation();
  }

  void _loadData() async {
    isFetching = true;
    setState(() {});
    showLoading(context);
    listOfData.clear();
    return getImagesFromDB().then((DataSnapshot event) {
      for (var value in event.value.values) {
        print('-->>>$value');
        listOfData.add(MemeData.fromJson(value));
      }

      for (int i = 0; i < listOfData.length; i++) {
        listOfData[i].key = event.value.entries.toList()[i].key;
      }
      Navigator.pop(context);
      isFetching = false;
      setState(() {});
    });
  }

  void shareImage(String url) async {
    showLoading(context);
    String path = await downloadImage(url);
    if (path != null) {
      await File(path).readAsBytes().then((value) async {
        Navigator.pop(context);
        await WcFlutterShare.share(
            sharePopupTitle: 'Share Image',
            subject: 'Memeloard',
            text: 'Memeloard an app for memes.',
            fileName: 'share.png',
            mimeType: 'image/png',
            bytesOfFile: Uint8List.fromList(value));
      });
    }
  }

  Future<String> downloadImage(String imageUrl) async {
    String dir = await getDir();
    var shareImage = "";
    var response = await HttpClient().getUrl(Uri.parse(imageUrl)).then((HttpClientRequest request) => request.close());

    shareImage = 'shareImage.png';
    var result = await response.pipe(new File(dir + "/" + shareImage).openWrite());
    return new File(dir + '/shareImage.png').path;
  }

  getDir() async {
    if (Platform.isIOS) return (await getTemporaryDirectory()).path;
    return (await getApplicationDocumentsDirectory()).path;
  }
}
