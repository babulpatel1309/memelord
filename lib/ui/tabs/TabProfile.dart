import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:memelord/data/card_model.dart';
import 'package:memelord/data/memedata.dart';
import 'package:memelord/utils/colors.dart';
import 'package:memelord/utils/constants.dart';
import 'package:memelord/utils/firebaseutils.dart';
import 'package:memelord/utils/util.dart';

import '../../utils/images.dart';

class TabProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TabProfileState();
  }
}

class TabProfileState extends State<TabProfile> with AutomaticKeepAliveClientMixin {
  List<MemeData> listOfData = List();
  MemeData selectedMeme;
  var memeTitles = List<String>(20);
  var isFetching = false;
  @override
  void initState() {
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            child: Image.asset(
              icLeaderBoard,
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            top: 60,
            left: 0,
            right: 0,
            child: Center(
              child: Image.asset(
                memelordTitle,
                width: MediaQuery.of(context).size.width * 0.6,
              ),
            ),
          ),
          _content()
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  _content() {
    return Container(
      margin: EdgeInsets.only(top: 100),
      child: listOfData.isEmpty
          ? Center(
              child: (isFetching
                  ? Container()
                  : Text(
                      'No records found',
                      style: TextStyle(color: skyBlue, fontSize: 50.0),
                    )),
            )
          : ListView(
              children: _getListItems(),
            ),
    );
  }

  _getLeaderBoardView() {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 10,
        ),
        Column(
          children: <Widget>[
            Image.asset(
              icDislike,
              width: 70,
            ),
            Text(
              "${selectedMeme != null ? selectedMeme.dislikes.toString() : 0} dies",
              style: TextStyle(color: black, fontWeight: FontWeight.w600, fontSize: 15),
            ),
          ],
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Container(
            child: Card(
              elevation: 5,
              clipBehavior: Clip.hardEdge,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Container(
                height: 150,
                child: CachedNetworkImage(
                  imageUrl: selectedMeme != null ? selectedMeme.imageURL : "",
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => SizedBox(
                    width: 48,
                    height: 48,
                    child: Center(child: CircularProgressIndicator()),
                  ),
                  errorWidget: (context, url, error) => SizedBox(
                    width: 48,
                    height: 48,
                    child: Icon(Icons.error),
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Column(
          children: <Widget>[
            Image.asset(
              icLike,
              width: 70,
            ),
            Text(
              "${selectedMeme != null ? selectedMeme.likes.toString() : 0} likes",
              style: TextStyle(color: black, fontWeight: FontWeight.w600, fontSize: 15),
            ),
          ],
        ),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  _getListItems() {
    currentIndex = 0;
    var listOfWidgets = List<Widget>();

    if (listOfData.length <= 0) return listOfWidgets;

//    listOfWidgets.add(_getLeaderBoardView());

    /*listOfWidgets.add(Center(
        child: Padding(
      padding: const EdgeInsets.all(12.0),
      child: Text(
        'Rating',
        style: TextStyle(fontWeight: FontWeight.bold, color: black, fontSize: 22),
      ),
    )));*/

    listOfData.asMap().forEach((index, data) {
      Gradient gradient;
      bool isLeading = true;
      if (index == 0) {
        gradient =  yellowGradient;
      } else if (index == 1) {
        gradient = silverGradient;
      } else if (index == 2) {
        gradient = orangeGradient;
      }else{
        gradient = whiteGradient;
        isLeading = false;
      }
      listOfWidgets.add(_getListTile(data, isLeading,gradient));
      currentIndex++;
    });

    return listOfWidgets;
  }

  int currentIndex = 0;

  _getListTile(final MemeData itemData, bool showLeaderIcon, Gradient gradient) {
    return InkWell(
      onTap: () {
        setState(() {
          selectedMeme = itemData;
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            gradient: gradient,
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: 40,
                  height: 40,
                  child: Center(
                    child: showLeaderIcon
                        ? Image.asset(
                            icReward,
                          )
                        : Text(
                            currentIndex.toString(),
                            style: TextStyle(color: black, fontWeight: FontWeight.w600, fontSize: 20),
                          ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Card(
                          elevation: 5,
                          clipBehavior: Clip.hardEdge,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                          child: Container(
                            width: 100,
                            height: 100,
                            child: CachedNetworkImage(
                              imageUrl: itemData.imageURL,
                              imageBuilder: (context, imageProvider) => Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              placeholder: (context, url) => SizedBox(
                                width: 48,
                                height: 48,
                                child: Center(child: CircularProgressIndicator()),
                              ),
                              errorWidget: (context, url, error) =>
                                  SizedBox(width: 48, height: 48, child: Icon(Icons.error)),
                            ),
                          ))
                    ],
                  ),
                ),
                Image.asset(
                  icLike,
                  width: 50,
                ),
                Text(
                  itemData.likes.toString(),
                  style: TextStyle(color: black, fontWeight: FontWeight.w600, fontSize: 15),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  memeTitles[itemData.index],
                  style: TextStyle(color: black, fontSize: 15),
                ),
                SizedBox(
                  width: 5,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _loadData() async {
    isFetching = true;
    setState(() {});
    listOfData.clear();

    await fetchTendingEntry().then((DataSnapshot event) {
      if (event.value != null) {
        for (var value in event.value.values) {
          memeTitles[value[PARAM_UID] - 1] = value[PARAM_TRENDING];
        }
      }
    });
    return getImagesFromDB().then((DataSnapshot event) {
      for (var value in event.value.values) {
        print('-->>>$value');
        listOfData.add(MemeData.fromJson(value));
      }

      for (int i = 0; i < listOfData.length; i++) {
        listOfData[i].key = event.value.entries.toList()[i].key;
      }
      listOfData.sort((a, b) {
        return b.likes.compareTo(a.likes);
      });

      int index = 0;
      listOfData.forEach((item) {
        item.index = index++;
      });

      selectedMeme = listOfData[0];
      isFetching = false;
      setState(() {});
    });
  }
}
