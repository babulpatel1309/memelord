import 'dart:io';
import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memelord/data/card_model.dart';
import 'package:memelord/data/memedata.dart';
import 'package:memelord/utils/constants.dart';
import 'package:memelord/utils/firebaseutils.dart';
import 'package:memelord/utils/util.dart';

import '../../utils/images.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_core/firebase_core.dart';

class TabTrending extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TabTrendingState();
  }
}

class TabTrendingState extends State<TabTrending> with AutomaticKeepAliveClientMixin {
//  List<CardModel> listOfData;
  var listOfData = List<MemeData>();
  var isFetching = false;
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

//    listOfData = generateDummyData();

    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            child: Image.asset(
              icProfileBG,
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            top: 60,
            left: 0,
            right: 0,
            child: Center(
              child: Image.asset(
                memelordTitle,
                width: MediaQuery.of(context).size.width * 0.6,
              ),
            ),
          ),
          _content(),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              margin: EdgeInsets.only(right: 16, bottom: 10),
              child: InkWell(
                onTap: _onAddClick,
                child: Image.asset(
                  icaAdd,
                  height: 80,
                  width: 80,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _content() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 60,
          ),
          Center(
            child: Image.asset(
              memelordTitle,
              width: MediaQuery.of(context).size.width * 0.6,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  GridView.count(
                    crossAxisSpacing: 10,
                    primary: false,
                    shrinkWrap: true,
                    crossAxisCount: 3,
                    children: List.generate(listOfData.length, (index) {
                      return _gridItem(index);
                    }),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _gridItem(index) {
    return Container(
        child: Card(
      elevation: 5,
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
      child: CachedNetworkImage(
        imageUrl: listOfData[index].imageURL,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        placeholder: (context, url) => SizedBox(
          width: 48,
          height: 48,
          child: Center(child: CircularProgressIndicator()),
        ),
        errorWidget: (context, url, error) => SizedBox(
          width: 48,
          height: 48,
          child: Icon(Icons.error),
        ),
      ),
    ));
  }

  _onAddClick() async {
    bool isLoggedIn = await checkLoggedinUser(context);
    if (isLoggedIn) {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery, imageQuality: 50);
      if (image == null) return;
      showLoading(context);
      await uploadImage(image);
      _loadData();
      Navigator.pop(context);
    }
  }

  void _loadData() async {
    isFetching = true;
    setState(() {});
    listOfData.clear();
    return getImagesForUserDB().then((DataSnapshot event) {
      if (event.value != null) {
        for (var value in event.value.values) {
          listOfData.add(MemeData.fromJson(value));
        }

        listOfData.sort((value1, value2) {
          return value1.likes > value2.likes ? 1 : -1;
        });

        listOfData.sort((a, b) {
          return b.likes.compareTo(a.likes);
        });
      }
      isFetching = false;
      setState(() {});
    });
  }
}
