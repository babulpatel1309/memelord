import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:memelord/data/card_model.dart';
import 'package:memelord/data/memedata.dart';

import '../../utils/colors.dart';

Positioned cardDemo(
    MemeData img,
    double bottom,
    double right,
    double left,
    double cardWidth,
    double rotation,
    double skew,
    BuildContext context,
    Function dismissImg,
    int flag,
    Function addImg,
    Function swipeRight,
    Function swipeLeft) {
  Size screenSize = MediaQuery.of(context).size;
  var cardHeight = screenSize.height / 2.9 + 80;
  // print("Card");
  return Positioned(
    top: (screenSize.height) * 0.01,
    right: flag == 0 ? right != 0.0 ? right : null : null,
    left: flag == 1 ? right != 0.0 ? right : null : null,
    bottom: (screenSize.height) * 0.01,
    child: Dismissible(
      key: new Key(new Random().toString()),
      crossAxisEndOffset: 0.3,
      onResize: () {
        //print("here");
        // setState(() {
        //   var i = data.removeLast();

        //   data.insert(0, i);
        // });
      },
      onDismissed: (DismissDirection direction) {
//          _swipeAnimation();
        if (direction == DismissDirection.endToStart)
          dismissImg(img);
        else
          addImg(img);
      },
      child: new Transform(
        alignment: flag == 0 ? Alignment.bottomRight : Alignment.bottomLeft, //transform: null,
        transform: new Matrix4.skewX(skew), //        ..rotateX(-math.pi / rotation),
        child: new RotationTransition(
          turns: new AlwaysStoppedAnimation(flag == 0 ? rotation / 360 : -rotation / 360),
          child: Container(
            child: Card(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: 5),
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: new Container(
                alignment: Alignment.center,
                width: screenSize.width / 1.2,
                child: new Container(
                  width: screenSize.width / 1.2,
                  height: screenSize.height / 1.77,
                  margin: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 8.0),
                  child: ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: CachedNetworkImage(
                        imageUrl: img.imageURL,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        placeholder: (context, url) => SizedBox(
                          width: 48,
                          height: 48,
                          child: Center(child: CircularProgressIndicator()),
                        ),
                        errorWidget: (context, url, error) => SizedBox(width: 48, height: 48, child: Icon(Icons.error)),
                      )),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
}

Positioned cardNotActive(
  MemeData img,
  double bottom,
  double right,
  double left,
  double cardWidth,
  double rotation,
  double skew,
  BuildContext context,
) {
  Size screenSize = MediaQuery.of(context).size;
  var cardHeight = screenSize.height / 2.9 + 80;
  // print("Card");
  return Positioned(
    top: (screenSize.height) * 0.01,
    right: 12.0,
    left: 12.0,
    bottom: 5,
    child: Dismissible(
      key: new Key(new Random().toString()),
      crossAxisEndOffset: 0.3,
      onResize: () {
        //print("here");
        // setState(() {
        //   var i = data.removeLast();

        //   data.insert(0, i);
        // });
      },
      onDismissed: (DismissDirection direction) {
//          _swipeAnimation();
        /*if (direction == DismissDirection.endToStart)
          dismissImg(img);
        else
          addImg(img);*/
      },
      child: Card(
        color: Colors.white,
        margin: EdgeInsets.only(bottom: 5, left: 10, right: 10),
        elevation: 0.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: new Container(
          alignment: Alignment.center,
          width: screenSize.width / 1.4,
          child: new Container(
            width: screenSize.width / 1.4,
            height: screenSize.height / 1.77,
            margin: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0, bottom: 8.0),
            child: ClipRRect(
              borderRadius: new BorderRadius.circular(8.0),
              child: CachedNetworkImage(
                imageUrl: img.imageURL,
                placeholder: (context, url) => SizedBox(
                  width: 48,
                  height: 48,
                  child: Center(child: CircularProgressIndicator()),
                ),
                errorWidget: (context, url, error) => SizedBox(width: 48, height: 48, child: Icon(Icons.error)),
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
