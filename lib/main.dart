import 'package:flutter/material.dart';
import 'package:memelord/ui/Dashboard.dart';
import 'package:memelord/ui/RegisterScreen.dart';
import 'package:memelord/ui/SplashScreen.dart';
import 'package:memelord/utils/routes.dart';


void main() => runApp(Memelord());

class Memelord extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Memelord',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
      onGenerateRoute: (RouteSettings settings) {
        var routes = <String, WidgetBuilder>{
          splashScreen: (ctx) => SplashScreen(),
          registerScreen: (ctx) => RegisterScreen(),
          dashboardScreen: (ctx) => DashboardScreen(),
        };

        var builder = routes[settings.name];
        return MaterialPageRoute(builder: (ctx) => builder(ctx));
      },
    );
  }
}
