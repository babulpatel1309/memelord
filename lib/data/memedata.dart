import 'package:firebase_database/firebase_database.dart';

class MemeData {
  int index ;
  String telNo;
  String uid;
  String imageURL;
  int likes;
  int dislikes;
  int timestamp;
  String key;

  MemeData(
      {this.telNo,
      this.uid,
      this.imageURL,
      this.likes,
      this.dislikes,
      this.timestamp,
      this.key});

  MemeData.fromJson(var json) {
    telNo = json['telNo'];
    uid = json['uid'];
    imageURL = json['imageURL'];
    likes = json['likes'];
    dislikes = json['dislikes'];
    timestamp = json['timestamp'];
  }

  MemeData.fromSnapshot(DataSnapshot json) {
    telNo = json.value['telNo'];
    uid = json.value['uid'];
    imageURL = json.value['imageURL'];
    likes = json.value['likes'];
    dislikes = json.value['dislikes'];
    timestamp = json.value['timestamp'];
  }

/*   toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['telNo'] = this.telNo;
    data['uid'] = this.uid;
    data['imageURL'] = this.imageURL;
    data['likes'] = this.likes;
    data['dislikes'] = this.dislikes;
    data['timestamp'] = this.timestamp;
    return data;
  }*/

  toJson() {
    return {
      'telNo': this.telNo,
      'uid': this.uid,
      'imageURL': this.imageURL,
      'likes': this.likes,
      'dislikes': this.dislikes,
      'timestamp': this.timestamp
    };
  }
}
