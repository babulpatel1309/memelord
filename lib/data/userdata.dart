class UserData {
  String telNo;
  String uid;

  UserData({this.telNo, this.uid});

  UserData.fromJson(Map<String, dynamic> json) {
    telNo = json['telNo'];
    uid = json['uid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['telNo'] = this.telNo;
    data['uid'] = this.uid;
    return data;
  }
}
